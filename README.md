# Ohm
## A secure, zero-knowledge, ephemeral organizing app




### Core Features
1. Users can attend events anonymously, with no need to create an Ohm account
  - Anyone with the public URL for an event can attend
2. No centralized identifiable user data, account creation process is as follows:
  - An ssh key is generated and stored on their device, to be used for logging in to the service (no passwords)
  - A public/private RSA keypair is generated and stored on their device, to be used for making connections
  - A UUID is generated and stored on their device, to be used for tracking attendance at events
  - The app never knows any personal user data
3. Event details may be obfuscated by the organizer (location, purpose, time, etc)
4. A user sees all attendees as anonymous, unless they are connected
  - To connect with another user, exchange public keys
  - Use the other user's public key to encrypt a hash of your "UUID:Full Name"
  - App will compare attending list of UUIDs to local device's connections, display name if match (this serves as social validation, so a user can know if their friends are attending)
5. Event details are stored ephemerally, and encrypted using (at a minimum) the bcrypt standard
  - Users may keep local, encrypted records of events (with no attendee data)
  - Once an event is over, the data on Ohm's servers is deleted


### Non-critical Features
1. Private messaging using the Signal protocol
2. Encrypted file sharing/collaboration, perhaps using git


### Technical Concerns
1. Likely needs to be an OS-level application (I'm unsure whether a web app could securely/feasibly handle key exchanges, file system read/writes)
2. Cross-platform language/framework or OS-specific ones?
3. Need to decide on an encryption standard
4. Need flow for user adding a new device, switching device (key updating, alerting connections, etc)
5. Need protections against URL crawling
6. Need strategy to reduce metadata, key association and patterns (e.g. these UUIDs attend the same events)
7. Lock out users after period of inactivity
8. Need flow for easy user connection (similar to PGP contact list)
